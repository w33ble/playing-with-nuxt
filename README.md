# nuxt

> Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).


## Development

The server is just a `json-server` instance, and can be started with the following:

```
yarn run server
```

Once that's running, you can start the nuxt dev server and get to work. It will hot reload as you work.

```
yarn run dev
```