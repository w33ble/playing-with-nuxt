import fetch from '~/lib/fetch';

export const state = () => ({
  list: [],
  user: null,
});

export const mutations = {
  populate(state, users) {
    state.list.splice(0, 0, ...users);
  },

  set(state, user) {
    state.user = user;
  },
};

export const actions = {
  async fetch({ commit }) {
    const users = await fetch.get('/users').then(res => res.data);
    commit('populate', users);
  },

  async get({ commit }, id) {
    const user = await fetch.get(`/users/${id}`).then(res => res.data);
    commit('set', user);
  },
};

export const getters = {
  count(state) {
    return state.list.length;
  },
};
