import fetch from '~/lib/fetch';

export const state = () => ({
  list: [],
});

export const mutations = {
  populate(state, posts) {
    state.list.splice(0, 0, ...posts);
  },

  set(state, idx, post) {
    state.user.splice(idx, 1, post);
  },
};

export const actions = {
  async getByUser({ commit }, userId) {
    const posts = await fetch(`/posts?userId=${userId}`).then(res => res.data);
    commit('populate', posts);
  },
};

export const getters = {
  count(state) {
    return state.list.length;
  },
};
